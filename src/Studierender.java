import java.util.Date;

public class Studierender implements Comparable<Studierender>{

	private String name;
	private int matrikelnr;
	private Date geburtsDatum;
	
	public Studierender(String name, int matrikelnr, Date geburtsDatum) {
		
		this.name = name;
		this.matrikelnr = matrikelnr;
		this.geburtsDatum = geburtsDatum;
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMatrikelnr() {
		return matrikelnr;
	}
	public void setMatrikelnr(int matrikelnr) {
		this.matrikelnr = matrikelnr;
	}
	public Date getGeburtsDatum() {
		return geburtsDatum;
	}
	public void setGeburtsDatum(Date geburtsDatum) {
		this.geburtsDatum = geburtsDatum;
	}
	
	@Override
	public int compareTo(Studierender arg0) {
		int result = 0;
		
		result = this.name.compareTo(arg0.name);
		
		if (result == 0) {
			
			result = Integer.compare(this.matrikelnr, arg0.matrikelnr);
			
		}
		
		if (result == 0) {
			
			result = this.geburtsDatum.compareTo(arg0.geburtsDatum);
			
		}
		
		return result;
	}
	
	public String toString() {
		
		return "[" + name + " " + matrikelnr + " " + geburtsDatum.toString() + "]";
		
	}
	
}
