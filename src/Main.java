import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Main {

	public static void main(String[] args) throws ParseException {
		
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMAN);
		
		List<Studierender> array = new ArrayList<Studierender>();
				
		System.out.println(mergeSort(array));
		
	}
	
	public static List<Studierender> mergeSort(List<Studierender> array) {
		
		List<Studierender> result = new ArrayList<Studierender>();
		
		if (array.isEmpty()) {
			return new ArrayList<Studierender>(array);
		}
		
		if (array.size() == 1) {
			return new ArrayList<Studierender>(array);
		}
		
		List<Studierender> half1 = new ArrayList<Studierender>();
		List<Studierender> half2 = new ArrayList<Studierender>();
		
		for (int i = 0; i < array.size(); i++) {
			
			if (i < array.size() / 2) {
				half1.add(array.get(i));
			}
			else {
				half2.add(array.get(i));
			}
			
		}
		
		result = merge(mergeSort(half1), mergeSort(half2));
		
		return result;
		
	}
	
	public static List<Studierender> merge(List<Studierender> a1, List<Studierender> a2) {
		
		List<Studierender> result = new ArrayList<Studierender>();
		
		while (!a1.isEmpty() || !a2.isEmpty()) {
			
			if (a1.isEmpty()) {
				result.addAll(a2);
				break;
			}
			if (a2.isEmpty()) {
				result.addAll(a1);
				break;
			}
			
			if (a1.get(0).compareTo(a2.get(0)) <= 0) {
				
				result.add(a1.remove(0));
				
			}
			else {
				
				result.add(a2.remove(0));
				
			}
			
		}
		
		return result;
		
	}
	
}
